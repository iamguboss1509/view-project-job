import NotFound from "@/components/GeneralViews/NotFoundPage";
import DashboardLayout from "@/components/Dashboard/Layout/DashboardLayout";

const routes = [
    {
        path: '/',
        component: DashboardLayout,
        redirect: '/applicants',
        children: [
            {
                name: 'Applicants',
                path: '/applicants',
                component: () => import('../views/applicants/index')
            },
            {
                name: 'FromApplicants',
                path: '/applicants/form',
                component: () => import('../views/applicants/personal_form')
            },
            {
                name: 'Organization',
                path: '/company',
                component: () => import('../views/organizations/index')
            },
            {
                name: 'contactus',
                path: '/contactus',
                component: () => import('../views/contactus')
            }
        ]
    },
    {path: '*', component: NotFound}
]

export default routes
